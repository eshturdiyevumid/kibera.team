<?php

return [

    /*
    |--------------------------------------------------------------------------
    | View Storage Paths
    |--------------------------------------------------------------------------
    |
    | Most templating systems load templates from disk. Here you may specify
    | an array of paths that should be checked for your views. Of course
    | the usual Laravel view path has already been registered for you.
    |
    */
    'menu' => [[
        'icon' => 'fa fa-th-large',
        'title' => 'Bosh sahifa',
        'url' => '/dashboard/v3',
    ], [
        'icon' => 'fa fa-hdd',
        'title' => 'Kontaktlar',
        'url' => '/contacts',
    ], [
        'icon' => 'fa fa-list-ol',
        'title' => 'Ssenariylar',
        'url' => '/scenario/index',
    ], [
        'icon' => 'fa fa-tag',
        'title' => 'Tarif rejalar',
        'url' => '/tariffs',
    ], [
        'icon' => 'fa fa-comments',
        'title' => 'Admin bilan aloqa',
        'url' => '/chats',
    ], [
        'icon' => 'fa fa-user',
        'title' => 'Kompaniya ma\'lumotlari',
        'url' => '/company',
    ], [
        'icon' => 'fa fa-cogs',
        'title' => 'Sozlamalar',
        'url' => '/form/wizards',
    ]
    ]
];
