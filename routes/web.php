<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ScenarioController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\TariffController;
use App\Http\Controllers\ChatController;


Route::get('/', function () {
	return redirect('/dashboard/v3');
});

Route::get('/dashboard/v3', function () {
    return view('pages/dashboard-v3');
})->name('dashboard');

// Scenario
Route::name('scenario.')->prefix('/scenario')->group(function () {
    Route::get('/index', [ScenarioController::class, 'index'])->name('index');
//    Route::get('/create', function () {
//        return view('flowchart/index');
//    })->name('create');
    Route::get('/create', [ScenarioController::class, 'create'])->name('create');
});

Route::get('/contacts', [ContactController::class, 'index'])->name('contact.index');
Route::get('/tariffs', [TariffController::class, 'index'])->name('tariff.index');
Route::get('/chats', [ChatController::class, 'index'])->name('chat.index');
Route::get('/company', function () {
    return view('pages/Company/index');
})->name('company.index');
