<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ScenarioController extends Controller
{
    public function index()
    {
        return view('pages/Scenario/index');
    }

    public function create()
    {
        return view('/pages/Scenario/create');
    }
}
