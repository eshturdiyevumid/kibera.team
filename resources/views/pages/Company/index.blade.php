@extends('layouts.default')

@section('title', 'Wizards')

@push('css')
	<link href="/assets/plugins/smartwizard/dist/css/smart_wizard.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Bosh sahifa</a></li>
        <li class="breadcrumb-item active">Kompaniya ma'lumotlari</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Kompaniya ma'lumotlari</h1>
	<!-- end page-header -->
	<!-- begin wizard-form -->
	<form action="/" method="POST" class="form-control-with-bg">
		<!-- begin wizard -->
		<div id="wizard">
			<!-- begin wizard-step -->
			<ul>
				<li>
					<a href="#step-1">
						<span class="number">1</span>
						<span class="info">
							Umumiy ma'lumotlar
							<small>Nomi, Address, IC No and DOB</small>
						</span>
					</a>
				</li>
				<li>
					<a href="#step-2">
						<span class="number">2</span>
						<span class="info">
							Kontakt ma'lumotlari
							<small>Email va telefon</small>
						</span>
					</a>
				</li>
				<li>
					<a href="#step-3">
						<span class="number">3</span>
						<span class="info">
							Login Account
							<small>Foydalanuvchi nomi va paroll</small>
						</span>
					</a>
				</li>
			</ul>
			<!-- end wizard-step -->
			<!-- begin wizard-content -->
			<div>
				<!-- begin step-1 -->
				<div id="step-1">
					<!-- begin fieldset -->
					<fieldset>
						<!-- begin row -->
						<div class="row">
							<!-- begin col-8 -->
							<div class="col-xl-8 offset-xl-2">
								<legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Umumiy ma'lumotlar</legend>
								<!-- begin form-group row -->
								<div class="form-group row m-b-10">
									<label class="col-lg-3 text-lg-right col-form-label">Nomi</label>
									<div class="col-lg-9 col-xl-6">
										<input type="text" name="firstname" placeholder="Kibera" class="form-control" />
									</div>
								</div>
								<!-- end form-group row -->
								<!-- begin form-group row -->
{{--								<div class="form-group row m-b-10">--}}
{{--									<label class="col-lg-3 text-lg-right col-form-label">Familyasi</label>--}}
{{--									<div class="col-lg-9 col-xl-6">--}}
{{--										<input type="text" name="lastname" placeholder="Smith" class="form-control" />--}}
{{--									</div>--}}
{{--								</div>--}}
								<!-- end form-group row -->
								<!-- begin form-group row -->
								<div class="form-group row m-b-10">
									<label class="col-lg-3 text-lg-right col-form-label">Tashkil topgan sanasi</label>
									<div class="col-lg-9 col-xl-6">
										<div class="row row-space-6">
											<div class="col-4">
												<select class="form-control" name="year">
													<option>-- Yil --</option>
												</select>
											</div>
											<div class="col-4">
												<select class="form-control" name="month">
													<option>-- Oy --</option>
												</select>
											</div>
											<div class="col-4">
												<select class="form-control" name="day">
													<option>-- Kun --</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<!-- end form-group row -->
								<!-- begin form-group row -->
								<div class="form-group row m-b-10">
									<label class="col-lg-3 text-lg-right col-form-label">IC No</label>
									<div class="col-lg-9 col-xl-6">
										<input type="text" name="ic" placeholder="" class="form-control" />
									</div>
								</div>
								<!-- end form-group row -->
								<!-- begin form-group row -->
								<div class="form-group row m-b-10">
									<label class="col-lg-3 text-lg-right col-form-label">Address</label>
									<div class="col-lg-9">
										<input type="text" name="address1" placeholder="Address 1" class="form-control m-b-10" />
										<input type="text" name="address2" placeholder="Address 2" class="form-control" />
									</div>
								</div>
								<!-- end form-group row -->
							</div>
							<!-- end col-8 -->
						</div>
						<!-- end row -->
					</fieldset>
					<!-- end fieldset -->
				</div>
				<!-- end step-1 -->
				<!-- begin step-2 -->
				<div id="step-2">
					<!-- begin fieldset -->
					<fieldset>
						<!-- begin row -->
						<div class="row">
							<!-- begin col-8 -->
							<div class="col-xl-8 offset-xl-2">
								<legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">You contact info, so that we can easily reach you</legend>
								<!-- begin form-group row -->
								<div class="form-group row m-b-10">
									<label class="col-lg-3 text-lg-right col-form-label">Phone Number</label>
									<div class="col-lg-9 col-xl-6">
										<input type="number" name="phone" placeholder="123-456-7890" class="form-control" />
									</div>
								</div>
								<!-- end form-group row -->
								<!-- begin form-group row -->
								<div class="form-group row m-b-10">
									<label class="col-lg-3 text-lg-right col-form-label">Email Address</label>
									<div class="col-lg-9 col-xl-6">
										<input type="email" name="email" placeholder="someone@example.com" class="form-control" />
									</div>
								</div>
								<!-- end form-group row -->
							</div>
							<!-- end col-8 -->
						</div>
						<!-- end row -->
					</fieldset>
					<!-- end fieldset -->
				</div>
				<!-- end step-2 -->
				<!-- begin step-3 -->
				<div id="step-3">
					<!-- begin fieldset -->
					<fieldset>
						<!-- begin row -->
						<div class="row">
							<!-- begin col-8 -->
							<div class="col-xl-8 offset-xl-2">
								<legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Select your login username and password</legend>
								<!-- begin form-group row -->
								<div class="form-group row m-b-10">
									<label class="col-lg-3 text-lg-right col-form-label">Username</label>
									<div class="col-lg-9 col-xl-6">
										<input type="text" name="username" placeholder="johnsmithy" class="form-control" />
									</div>
								</div>
								<!-- end form-group row -->
								<!-- begin form-group row -->
								<div class="form-group row m-b-10">
									<label class="col-lg-3 text-lg-right col-form-label">Pasword</label>
									<div class="col-lg-9 col-xl-6">
										<input type="password" name="password" placeholder="Your password" class="form-control" />
									</div>
								</div>
								<!-- end form-group row -->
								<!-- begin form-group row -->
								<div class="form-group row m-b-10">
									<label class="col-lg-3 text-lg-right col-form-label">Confirm Pasword</label>
									<div class="col-lg-9 col-xl-6">
										<input type="password" name="password2" placeholder="Confirmed password" class="form-control" />
									</div>
								</div>
								<!-- end form-group row -->
							</div>
							<!-- end col-8 -->
						</div>
						<!-- end row -->
					</fieldset>
					<!-- end fieldset -->
				</div>
				<!-- end step-3 -->
			</div>
			<!-- end wizard-content -->
		</div>
		<!-- end wizard -->
	</form>
	<!-- end wizard-form -->
@endsection

@push('scripts')
	<script src="/assets/plugins/smartwizard/dist/js/jquery.smartWizard.js"></script>
	<script src="/assets/js/demo/form-wizards.demo.js"></script>
@endpush
