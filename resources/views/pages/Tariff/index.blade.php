@extends('layouts.default')

@section('title', 'Tarif rejalar')

@push('css')
	<link href="/assets/plugins/lightbox2/dist/css/lightbox.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Bosh sahifa</a></li>
        <li class="breadcrumb-item active">Tarif rejalar</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Tarif rejalar</h1>
	<!-- end page-header -->
	<!-- begin #options -->
	<div id="options" class="m-b-10">
		<span class="gallery-option-set" id="filter" data-option-key="filter">
		<a href="#show-all" class="btn btn-default btn-xs active" data-option-value="*">Barchasi</a>
		<a href="#gallery-group-1" class="btn btn-default btn-xs" data-option-value=".gallery-group-1">Aktiv holatdagilar</a>
{{--		<a href="#gallery-group-2" class="btn btn-default btn-xs" data-option-value=".gallery-group-2">Arxivdagilar</a>--}}
{{--		<a href="#gallery-group-3" class="btn btn-default btn-xs" data-option-value=".gallery-group-3">Tarix</a>--}}
{{--		<a href="#gallery-group-4" class="btn btn-default btn-xs" data-option-value=".gallery-group-4">Gallery Group 4</a>--}}
		</span>
	</div>
	<!-- end #options -->
	<!-- begin #gallery -->
	<div id="gallery" class="gallery">
		<!-- begin image -->
		<div class="image gallery-group-1">
			<div class="image-inner">
				<a href="/assets/img/gallery/gallery-1.jpg" data-lightbox="gallery-group-1">
					<div class="img" style="background-image: url(/assets/img/gallery/gallery-1.jpg)"></div>
				</a>
				<p class="image-caption">
					#1382
				</p>
			</div>
			<div class="image-info">
				<h5 class="title">Tarif</h5>
				<div class="rating pull-left">
                    <small>Narxi: 1 000 000 so'm</small>
				</div>
                <div class="pull-right">
                    <a href="#" class="btn btn-xs btn-primary">Sotib olish </a>
                </div>
                <div class="desc">

                </div>

			</div>
		</div>
		<!-- end image -->
        <div class="image gallery-group-1">
            <div class="image-inner">
                <a href="/assets/img/gallery/gallery-7.jpg" data-lightbox="gallery-group-1">
                    <div class="img" style="background-image: url(/assets/img/gallery/gallery-7.jpg)"></div>
                </a>
                <p class="image-caption">
                    #138
                </p>
            </div>
            <div class="image-info">
                <h5 class="title">Tarif 1</h5>
                <div class="rating pull-left">
                    <small>Narxi: 1 000 000 so'm</small>
                </div>
                <div class="pull-right">
                    <a href="#" class="btn btn-xs btn-primary">Sotib olish </a>
                </div>
                <div class="desc">

                </div>

            </div>
        </div>
        <div class="image gallery-group-1">
            <div class="image-inner">
                <a href="/assets/img/gallery/gallery-8.jpg" data-lightbox="gallery-group-1">
                    <div class="img" style="background-image: url(/assets/img/gallery/gallery-8.jpg)"></div>
                </a>
                <p class="image-caption">
                    #1383
                </p>
            </div>
            <div class="image-info">
                <h5 class="title">Tarif 2</h5>
                <div class="rating pull-left">
                    <small>Narxi: 1 000 000 so'm</small>
                </div>
                <div class="pull-right">
                    <a href="#" class="btn btn-xs btn-primary">Sotib olish </a>
                </div>
                <div class="desc">

                </div>

            </div>
        </div>
        <div class="image gallery-group-1">
            <div class="image-inner">
                <a href="/assets/img/gallery/gallery-4.jpg" data-lightbox="gallery-group-1">
                    <div class="img" style="background-image: url(/assets/img/gallery/gallery-4.jpg)"></div>
                </a>
                <p class="image-caption">
                    #1482
                </p>
            </div>
            <div class="image-info">
                <h5 class="title">Tarif 3</h5>
                <div class="rating pull-left">
                    <small>Narxi: 1 000 000 so'm</small>
                </div>
                <div class="pull-right">
                    <a href="#" class="btn btn-xs btn-primary">Sotib olish </a>
                </div>
                <div class="desc">

                </div>

            </div>
        </div>
        <div class="image gallery-group-1">
            <div class="image-inner">
                <a href="/assets/img/gallery/gallery-5.jpg" data-lightbox="gallery-group-1">
                    <div class="img" style="background-image: url(/assets/img/gallery/gallery-5.jpg)"></div>
                </a>
                <p class="image-caption">
                    #1582
                </p>
            </div>
            <div class="image-info">
                <h5 class="title">Tarif 4</h5>
                <div class="rating pull-left">
                    <small>Narxi: 1 000 000 so'm</small>
                </div>
                <div class="pull-right">
                    <a href="#" class="btn btn-xs btn-primary">Sotib olish </a>
                </div>
                <div class="desc">

                </div>

            </div>
        </div>
        <div class="image gallery-group-1">
            <div class="image-inner">
                <a href="/assets/img/gallery/gallery-6.jpg" data-lightbox="gallery-group-1">
                    <div class="img" style="background-image: url(/assets/img/gallery/gallery-6.jpg)"></div>
                </a>
                <p class="image-caption">
                    #1382
                </p>
            </div>
            <div class="image-info">
                <h5 class="title">Tarif 5</h5>
                <div class="rating pull-left">
                    <small>Narxi: 1 000 000 so'm</small>
                </div>
                <div class="pull-right">
                    <a href="#" class="btn btn-xs btn-primary">Sotib olish </a>
                </div>
                <div class="desc">

                </div>

            </div>
        </div>

    </div>
	<!-- end #gallery -->
@endsection

@push('scripts')
	<script src="/assets/plugins/isotope-layout/dist/isotope.pkgd.min.js"></script>
	<script src="/assets/plugins/lightbox2/dist/js/lightbox.min.js"></script>
	<script src="/assets/js/demo/gallery.demo.js"></script>
@endpush
