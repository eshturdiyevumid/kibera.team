@extends('layouts.default')

@section('title', 'Chat')

@push('css')
	<link href="/assets/plugins/nvd3/build/nv.d3.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Bosh sahifa</a></li>
        <li class="breadcrumb-item active">Admin aloqa</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">
		Admin bilan aloqa
	</h1>
	<!-- end page-header -->

	<span class="btn-group pull-right m-l-20 p-1 bg-black-transparent-2 rounded">
		<a href="#" class="btn btn-sm btn-white btn-white-without-border" data-change="widget-theme" data-theme="light"><i class="fa fa-sun text-blue"></i> Light</a>
		<a href="#" class="btn btn-sm btn-white btn-white-without-border" data-change="widget-theme" data-theme="dark"><i class="fa fa-moon"></i> Dark</a>
	</span>
	<p class="m-b-20">
		<br>
	</p>

	<!-- begin row -->
	<div class="row">
		<!-- begin col-4 -->
		<div class="col-lg-12 col-xl-12">
			<!-- begin row -->
			<div class="row">
				<!-- begin col-12 -->
				<div class="col-xl-12 col-lg-12">
					<!-- begin widget-chat -->
					<div class="m-b-10 f-s-10 m-t-10">
						<a href="#modal-widget-chat-input" class="pull-right f-s-10 text-grey-darker m-r-3 f-w-700" data-toggle="modal"></a>
						<b class="text-inverse">Admin aloqa</b>
					</div>
					<div class="widget-chat widget-chat-rounded m-b-30" data-id="widget">
						<!-- begin widget-chat-header -->
						<div class="widget-chat-header">
							<div class="widget-chat-header-icon">
								<i class="fab fa-earlybirds width-30 height-30 f-s-20 bg-yellow text-inverse text-center rounded-corner" style="line-height: 30px"></i>
							</div>
							<div class="widget-chat-header-content">
								<h4 class="widget-chat-header-title">Admin</h4>
{{--								<p class="widget-chat-header-desc">55 members, 4 online</p>--}}
							</div>
						</div>
						<!-- end widget-chat-header -->
						<!-- begin widget-chat-body -->
						<div class="widget-chat-body" data-scrollbar="true" data-height="300px">
							<!-- begin widget-chat-item -->
							<div class="widget-chat-item with-media left">
								<div class="widget-chat-media">
									<img alt="" src="/assets/img/user/user-1.jpg" />
								</div>
								<div class="widget-chat-info">
									<div class="widget-chat-info-container">
										<div class="widget-chat-name text-indigo">Hudson Mendes</div>
										<div class="widget-chat-message">Should we plan for a company trip this year?</div>
										<div class="widget-chat-time">6:00PM</div>
									</div>
								</div>
							</div>
							<!-- end widget-chat-item -->
							<!-- begin widget-chat-item -->
							<div class="widget-chat-item with-media left">
								<div class="widget-chat-media">
									<img alt="" src="/assets/img/user/user-2.jpg" />
								</div>
								<div class="widget-chat-info">
									<div class="widget-chat-info-container">
										<div class="widget-chat-name text-primary">Sam Sugerman</div>
										<div class="widget-chat-message">ok let's do it</div>
										<div class="widget-chat-time">6:01PM</div>
									</div>
								</div>
							</div>
							<!-- end widget-chat-item -->
							<!-- begin widget-chat-item -->
							<div class="widget-chat-item right">
								<div class="widget-chat-info">
									<div class="widget-chat-info-container">
										<div class="widget-chat-message">i'm ok with it</div>
										<div class="widget-chat-time">6:05PM</div>
									</div>
								</div>
							</div>
							<!-- end widget-chat-item -->
							<div class="text-center text-muted m-10 f-w-600">Today</div>
							<!-- begin widget-chat-item -->
							<div class="widget-chat-item with-media left">
								<div class="widget-chat-media">
									<img alt="" src="/assets/img/user/user-3.jpg" />
								</div>
								<div class="widget-chat-info">
									<div class="widget-chat-info-container">
										<div class="widget-chat-name text-orange">Jaxon Allwood</div>
										<div class="widget-chat-message">
											Here is some images for New Zealand
											<div class="row row-space-2 m-t-5">
												<div class="col-md-4">
													<a href="#" class="widget-card widget-card-sm square m-b-2">
														<div class="widget-card-cover" style="background-image: url(/assets/img/gallery/gallery-51-thumb.jpg)"></div>
													</a>
													<a href="#" class="widget-card widget-card-sm square m-b-2">
														<div class="widget-card-cover" style="background-image: url(/assets/img/gallery/gallery-52-thumb.jpg)"></div>
													</a>
												</div>
												<div class="col-md-4">
													<a href="#" class="widget-card widget-card-sm square m-b-2">
														<div class="widget-card-cover" style="background-image: url(/assets/img/gallery/gallery-53-thumb.jpg)"></div>
													</a>
													<a href="#" class="widget-card widget-card-sm square m-b-2">
														<div class="widget-card-cover" style="background-image: url(/assets/img/gallery/gallery-54-thumb.jpg)"></div>
													</a>
												</div>
												<div class="col-md-4">
													<a href="#" class="widget-card widget-card-sm square m-b-2">
														<div class="widget-card-cover" style="background-image: url(/assets/img/gallery/gallery-59-thumb.jpg)"></div>
													</a>
													<a href="#" class="widget-card widget-card-sm square m-b-2">
														<div class="widget-card-cover" style="background-image: url(/assets/img/gallery/gallery-60-thumb.jpg)"></div>
													</a>
												</div>
											</div>
										</div>
										<div class="widget-chat-time">6:20PM</div>
									</div>
								</div>
							</div>
							<!-- end widget-chat-item -->
						</div>
						<!-- end widget-chat-body -->
						<!-- begin widget-input -->
						<div class="widget-input widget-input-rounded">
							<form action="" method="POST" name="">
								<div class="widget-input-container">
									<div class="widget-input-icon"><a href="#" class="text-grey"><i class="fa fa-camera"></i></a></div>
									<div class="widget-input-box">
										<input type="text" class="form-control" placeholder="Write a message..." />
									</div>
									<div class="widget-input-icon"><a href="#" class="text-grey"><i class="fa fa-smile"></i></a></div>
									<div class="widget-input-divider"></div>
									<div class="widget-input-icon"><a href="#" class="text-grey"><i class="fa fa-microphone"></i></a></div>
								</div>
							</form>
						</div>
						<!-- end widget-input -->
					</div>
					<!-- end widget-chat -->
				</div>
				<!-- end col-12 -->
            </div>

	</div>
	<!-- end #modal-widget-table -->
@endsection

@push('scripts')
	<script src="/assets/plugins/d3/d3.min.js"></script>
	<script src="/assets/plugins/nvd3/build/nv.d3.js"></script>
	<script src="/assets/plugins/clipboard/dist/clipboard.min.js"></script>
	<script src="/assets/plugins/highlight.js/highlight.min.js"></script>
	<script src="/assets/js/demo/widget.demo.js"></script>
	<script src="/assets/js/demo/render.highlight.js"></script>
@endpush
