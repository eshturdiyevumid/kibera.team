@extends('layouts.default')

@section('title', 'Tabs & Accordions')

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Bosh sahifa</a></li>
        <li class="breadcrumb-item active">Kontaktlar</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Kontaktlar</h1>
	<!-- end page-header -->

	<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
		<div class="col-xl-12">
			<!-- begin nav-tabs -->
			<ul class="nav nav-tabs">
				<li class="nav-item">
					<a href="#default-tab-1" data-toggle="tab" class="nav-link active">
						<span class="d-sm-none">Tab 1</span>
						<span class="d-sm-block d-none">Guruhlar</span>
					</a>
				</li>
				<li class="nav-item">
					<a href="#default-tab-2" data-toggle="tab" class="nav-link">
						<span class="d-sm-none">Tab 2</span>
						<span class="d-sm-block d-none">Kontaktlar</span>
					</a>
				</li>
			</ul>
			<!-- end nav-tabs -->
			<!-- begin tab-content -->
			<div class="tab-content">
				<!-- begin tab-pane -->
				<div class="tab-pane fade active show" id="default-tab-1">
                    <div class="panel panel-inverse">
                        <!-- begin panel-heading -->
                        <div class="panel-heading">
                            <h4 class="panel-title">Guruhlar</h4>
                            <a href="#" class="btn btn-xs btn-primary mr-3">
                                <i class="fa fa-plus"></i> Yangi guruh yaratish </a>
                            <div class="panel-heading-btn">
{{--                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>--}}
{{--                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>--}}
{{--                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>--}}
{{--                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>--}}
                            </div>
                        </div>
                        <!-- end panel-heading -->
                        <!-- begin panel-body -->
                        <div class="panel-body">
                            <table id="data-table-default" class="table table-striped table-bordered table-td-valign-middle">

                                <thead>
                                <tr>
                                    <th width="1%"></th>
{{--                                    <th width="1%" data-orderable="false"></th>--}}
                                    <th class="text-nowrap">Nomi</th>
                                    <th class="text-nowrap">Biriktirilgan kontaktlar</th>
                                    <th class="text-nowrap">Yaratilgan vaqti</th>
                                    <th class="text-nowrap">Ammallar</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="odd gradeX">
                                    <td width="1%" class="f-s-600 text-inverse">1</td>
{{--                                    <td width="1%" class="with-img"><img src="/assets/img/user/user-1.jpg" class="img-rounded height-30" /></td>--}}
                                    <td>Jismoniy shaxslar</td>
                                    <td>550</td>
                                    <td>26.12.2021</td>
                                    <td class="text-center">
                                        <a href="#" class="btn  btn-icon btn-success mr-1"><span class="fas fa-eye "></span></a>
                                        <button type="button" class="btn btn-icon btn-primary mr-1 edit">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-icon btn-danger delete_button delete">
                                            <i class="fas fa-trash  text-white"></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr class="odd gradeX">
                                    <td width="1%" class="f-s-600 text-inverse">2</td>
                                    {{--                                    <td width="1%" class="with-img"><img src="/assets/img/user/user-1.jpg" class="img-rounded height-30" /></td>--}}
                                    <td>Yuridik shaxslar</td>
                                    <td>210</td>
                                    <td>25.12.2021</td>
                                    <td class="text-center">
                                        <a href="#" class="btn  btn-icon btn-success mr-1"><span class="fas fa-eye "></span></a>
                                        <button type="button" class="btn btn-icon btn-primary mr-1 edit">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-icon btn-danger delete_button delete">
                                            <i class="fas fa-trash  text-white"></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr class="odd gradeX">
                                    <td width="1%" class="f-s-600 text-inverse">3</td>
                                    {{--                                    <td width="1%" class="with-img"><img src="/assets/img/user/user-1.jpg" class="img-rounded height-30" /></td>--}}
                                    <td>Bayram aksiya ishtirokchilari</td>
                                    <td>124</td>
                                    <td>25.12.2021</td>
                                    <td class="text-center">
                                        <a href="#" class="btn  btn-icon btn-success mr-1"><span class="fas fa-eye "></span></a>
                                        <button type="button" class="btn btn-icon btn-primary mr-1 edit">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-icon btn-danger delete_button delete">
                                            <i class="fas fa-trash  text-white"></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr class="odd gradeX">
                                    <td width="1%" class="f-s-600 text-inverse">4</td>
                                    {{--                                    <td width="1%" class="with-img"><img src="/assets/img/user/user-1.jpg" class="img-rounded height-30" /></td>--}}
                                    <td>Yutuq egalari</td>
                                    <td>10</td>
                                    <td>26.12.2021</td>
                                    <td class="text-center">
                                        <a href="#" class="btn  btn-icon btn-success mr-1"><span class="fas fa-eye "></span></a>
                                        <button type="button" class="btn btn-icon btn-primary mr-1 edit">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-icon btn-danger delete_button delete">
                                            <i class="fas fa-trash  text-white"></i>
                                        </button>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <!-- end panel-body -->
                    </div>
                </div>
				<!-- end tab-pane -->
				<!-- begin tab-pane -->
				<div class="tab-pane fade" id="default-tab-2">
                    <div class="panel panel-inverse">
                        <!-- begin panel-heading -->
                        <div class="panel-heading">
                            <h4 class="panel-title">Umumiy kontaktlar</h4>
                            <a href="#" class="btn btn-xs btn-primary mr-3">
                                <i class="fa fa-plus"></i> Yangi kontakt yaratish </a>
                            <div class="panel-heading-btn">
                                {{--                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>--}}
                                {{--                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>--}}
                                {{--                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>--}}
                                {{--                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>--}}
                            </div>
                        </div>
                        <!-- end panel-heading -->
                        <!-- begin panel-body -->
                        <div class="panel-body">
                            <table id="data-table-default" class="table table-striped table-bordered table-td-valign-middle">

                                <thead>
                                <tr>
                                    <th width="1%"></th>
                                    {{--                                    <th width="1%" data-orderable="false"></th>--}}
                                    <th class="text-nowrap">FISH</th>
                                    <th class="text-nowrap">Telefon</th>
                                    <th class="text-nowrap">Jinsi</th>
                                    <th class="text-nowrap">Yoshi</th>
                                    <th class="text-nowrap">Guruhi</th>
                                    <th class="text-nowrap">Vaqt mintaqasi</th>
                                    <th class="text-nowrap">Ammallar</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="odd gradeX">
                                    <td width="1%" class="f-s-600 text-inverse">1</td>
                                    {{--                                    <td width="1%" class="with-img"><img src="/assets/img/user/user-1.jpg" class="img-rounded height-30" /></td>--}}
                                    <td>Azizov Anvar</td>
                                    <td>+998 90 687 25 31</td>
                                    <td>Erkak</td>
                                    <td>23</td>
                                    <td>Bayram aksiya ishtirokchilari</td>
                                    <td>GMT +5</td>
                                    <td class="text-center">
                                        <a href="#" class="btn  btn-icon btn-success mr-1"><span class="fas fa-eye "></span></a>
                                        <button type="button" class="btn btn-icon btn-primary mr-1 edit">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-icon btn-danger delete_button delete">
                                            <i class="fas fa-trash  text-white"></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr class="odd gradeX">
                                    <td width="1%" class="f-s-600 text-inverse">2</td>
                                    {{--                                    <td width="1%" class="with-img"><img src="/assets/img/user/user-1.jpg" class="img-rounded height-30" /></td>--}}
                                    <td>Karimov Latif</td>
                                    <td>+998 90 722 45 32</td>
                                    <td>Erkak</td>
                                    <td>22</td>
                                    <td>Jismoniy shaxslar</td>
                                    <td>GMT +5</td>
                                    <td class="text-center">
                                        <a href="#" class="btn  btn-icon btn-success mr-1"><span class="fas fa-eye "></span></a>
                                        <button type="button" class="btn btn-icon btn-primary mr-1 edit">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-icon btn-danger delete_button delete">
                                            <i class="fas fa-trash  text-white"></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr class="odd gradeX">
                                    <td width="1%" class="f-s-600 text-inverse">3</td>
                                    {{--                                    <td width="1%" class="with-img"><img src="/assets/img/user/user-1.jpg" class="img-rounded height-30" /></td>--}}
                                    <td>Azizov Anvar</td>
                                    <td>+998 90 654 56 66</td>
                                    <td>Erkak</td>
                                    <td>23</td>
                                    <td>Jismoniy shaxslar</td>
                                    <td>GMT +5</td>
                                    <td class="text-center">
                                        <a href="#" class="btn  btn-icon btn-success mr-1"><span class="fas fa-eye "></span></a>
                                        <button type="button" class="btn btn-icon btn-primary mr-1 edit">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-icon btn-danger delete_button delete">
                                            <i class="fas fa-trash  text-white"></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr class="odd gradeX">
                                    <td width="1%" class="f-s-600 text-inverse">4</td>
                                    {{--                                    <td width="1%" class="with-img"><img src="/assets/img/user/user-1.jpg" class="img-rounded height-30" /></td>--}}
                                    <td>Azizov Anvar</td>
                                    <td>+998 90 124 56 66</td>
                                    <td>Erkak</td>
                                    <td>23</td>
                                    <td>Bayram aksiya ishtirokchilari</td>
                                    <td>GMT +5</td>
                                    <td class="text-center">
                                        <a href="#" class="btn  btn-icon btn-success mr-1"><span class="fas fa-eye "></span></a>
                                        <button type="button" class="btn btn-icon btn-primary mr-1 edit">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-icon btn-danger delete_button delete">
                                            <i class="fas fa-trash  text-white"></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr class="odd gradeX">
                                    <td width="1%" class="f-s-600 text-inverse">5</td>
                                    {{--                                    <td width="1%" class="with-img"><img src="/assets/img/user/user-1.jpg" class="img-rounded height-30" /></td>--}}
                                    <td>Karimov Latif</td>
                                    <td>+998 90 124 56 66</td>
                                    <td>Erkak</td>
                                    <td>22</td>
                                    <td>Jismoniy shaxslar</td>
                                    <td>GMT +5</td>
                                    <td class="text-center">
                                        <a href="#" class="btn  btn-icon btn-success mr-1"><span class="fas fa-eye "></span></a>
                                        <button type="button" class="btn btn-icon btn-primary mr-1 edit">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-icon btn-danger delete_button delete">
                                            <i class="fas fa-trash  text-white"></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr class="odd gradeX">
                                    <td width="1%" class="f-s-600 text-inverse">6</td>
                                    {{--                                    <td width="1%" class="with-img"><img src="/assets/img/user/user-1.jpg" class="img-rounded height-30" /></td>--}}
                                    <td>Azizov Anvar</td>
                                    <td>+998 90 689 36 56</td>
                                    <td>Erkak</td>
                                    <td>23</td>
                                    <td>Bayram aksiya ishtirokchilari</td>
                                    <td>GMT +5</td>
                                    <td class="text-center">
                                        <a href="#" class="btn  btn-icon btn-success mr-1"><span class="fas fa-eye "></span></a>
                                        <button type="button" class="btn btn-icon btn-primary mr-1 edit">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-icon btn-danger delete_button delete">
                                            <i class="fas fa-trash  text-white"></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr class="odd gradeX">
                                    <td width="1%" class="f-s-600 text-inverse">7</td>
                                    {{--                                    <td width="1%" class="with-img"><img src="/assets/img/user/user-1.jpg" class="img-rounded height-30" /></td>--}}
                                    <td>Karimov Latif</td>
                                    <td>+998 90 782 14 56</td>
                                    <td>Erkak</td>
                                    <td>22</td>
                                    <td>Jismoniy shaxslar</td>
                                    <td>GMT +5</td>
                                    <td class="text-center">
                                        <a href="#" class="btn  btn-icon btn-success mr-1"><span class="fas fa-eye "></span></a>
                                        <button type="button" class="btn btn-icon btn-primary mr-1 edit">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-icon btn-danger delete_button delete">
                                            <i class="fas fa-trash  text-white"></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr class="odd gradeX">
                                    <td width="1%" class="f-s-600 text-inverse">8</td>
                                    {{--                                    <td width="1%" class="with-img"><img src="/assets/img/user/user-1.jpg" class="img-rounded height-30" /></td>--}}
                                    <td>Azizov Anvar</td>
                                    <td>+998 90 982 45 68</td>
                                    <td>Erkak</td>
                                    <td>23</td>
                                    <td>Jismoniy shaxslar</td>
                                    <td>GMT +5</td>
                                    <td class="text-center">
                                        <a href="#" class="btn  btn-icon btn-success mr-1"><span class="fas fa-eye "></span></a>
                                        <button type="button" class="btn btn-icon btn-primary mr-1 edit">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-icon btn-danger delete_button delete">
                                            <i class="fas fa-trash  text-white"></i>
                                        </button>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <!-- end panel-body -->
                    </div>
				</div>
				<!-- end tab-pane -->
				<!-- begin tab-pane -->
				<!-- end tab-pane -->
			</div>
			<!-- end tab-content -->
			<!-- begin nav-pills -->
			<!-- end nav-pills -->
			<!-- begin tab-content -->
			<!-- end tab-content -->
		</div>
		<!-- end col-6 -->
		<!-- begin col-6 -->
		<!-- end col-6 -->
	</div>
	<!-- end row -->
@endsection
