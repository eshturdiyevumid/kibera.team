@extends('layouts.default')

@section('title', 'Ssenariy yaratish')

@section('content')
	<!-- begin breadcrumb -->
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Bosh sahifa</a></li>
        <li class="breadcrumb-item"><a href="{{ route('scenario.index') }}">Ssenariylar</a></li>
        <li class="breadcrumb-item active">Ssenariy yaratish</li>
    </ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Ssenariy yaratish</h1>
	<!-- end page-header -->

	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="form-stuff-12">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<h4 class="panel-title">Ssenariy </h4>
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
                    <form action="" class="form-group" method="get" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-xl-6 col-md-6">
                                <div class="form-group">
                                    <label for="name">Ssenariy nomi</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-xl-6 col-md-6">
                                <div class="form-group">
                                    <label for="name">Ssenariy tasnifi</label>
                                    <textarea class="form-control"> </textarea>
                                </div>
                            </div>
{{--                            <div class="col-xl-2 col-md-6">--}}
{{--                                <button type="button" class="btn btn-primary mt-4" onclick="">--}}
{{--                                    <i class="fa fa-plus-square"></i> Blok qo'shish--}}
{{--                                </button>--}}
{{--                            </div>--}}
                        </div>
                        <div class="row">
                            <img src="/image/block.png" width="100%">
                        </div>
                        <br>
                            <a href="{{route('scenario.index')}}" class="btn btn-danger float-right">
                                <i class="fas fa-arrow-circle-left"></i>
                                Ortga
                            </a>
                            <a href="{{route('scenario.index')}}" class="btn btn-success float-right mr-2">
                                <i class="fas fa-arrow-circle-left"></i>
                                Saqlash
                            </a>
                            <a href="{{route('scenario.index')}}" class="btn btn-info float-right mr-2">
                                <i class="fas fa-arrow-circle-left"></i>
                                Chop etish
                            </a>
                    </form>

                </div>
				<!-- end panel-body -->
				<!-- end hljs-wrapper -->
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-6 -->
	</div>
	<!-- end row -->
@endsection

@push('scripts')
	<script src="/assets/plugins/highlight.js/highlight.min.js"></script>
	<script src="/assets/js/demo/render.highlight.js"></script>
@endpush
