@extends('layouts.default')

@section('title', 'Ssenariylar')

@push('css')
	<link href="/assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
	<link href="/assets/plugins/datatables.net-fixedcolumns-bs4/css/fixedcolumns.bootstrap4.min.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Bosh sahifa</a></li>
		<li class="breadcrumb-item active">Ssenariylar</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Ssenariylar</h1>
	<!-- end page-header -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-xl-12">
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<!-- begin panel-heading -->
				<div class="panel-heading">
					<h4 class="panel-title">Ssenariylar</h4>
                    <a href="{{ route('scenario.create') }}" class="btn btn-xs btn-primary mr-3">
                        <i class="fa fa-plus"></i> Yangi ssenariy </a>
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
				</div>
				<!-- end panel-heading -->
				<!-- begin panel-body -->
				<div class="panel-body">
					<table id="data-table-fixed-columns" class="table table-striped table-bordered table-td-valign-middle">
						<thead>
							<tr>
								<th width="1%"></th>
								<th class="text-nowrap">Nomi</th>
								<th class="text-nowrap">Status</th>
								<th class="text-nowrap">Yaratilgan vaqti</th>
								<th class="text-nowrap">O'zgartirilgan vaqti</th>
								<th class="text-nowrap">Amallar</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width="1%" class="f-s-600 text-inverse">1</td>
								<td>Aksiyalar</td>
								<td>Aktiv</td>
								<td>25.12.2021</td>
								<td>25.12.2021</td>
                                <td class="text-center">
                                    <a href="#" class="btn  btn-icon btn-success mr-1"><span class="fas fa-eye "></span></a>
                                    <button type="button" class="btn btn-icon btn-primary mr-1 edit">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                    <button type="button" class="btn btn-icon btn-danger delete_button delete">
                                        <i class="fas fa-trash  text-white"></i>
                                    </button>
                                </td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-10 -->
	</div>
	<!-- end row -->
@endsection

@push('scripts')
	<script src="/assets/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="/assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
	<script src="/assets/plugins/datatables.net-fixedcolumns/js/dataTables.fixedcolumns.min.js"></script>
	<script src="/assets/plugins/datatables.net-fixedcolumns-bs4/js/fixedcolumns.bootstrap4.min.js"></script>
	<script src="/assets/js/demo/table-manage-fixed-columns.demo.js"></script>
@endpush
